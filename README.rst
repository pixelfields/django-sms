=====
django sms
=====

Django sms is a simple api for django projects to send sms.

Quick start
-----------
1. Install it by pip:

    pip install https://bitbucket.org/pixelfields/django-sms/get/master.tar.gz

1. Add "django_pixelfields_sms" to INSTALLED_APPS:
    INSTALLED_APPS = {
        ...
        'django_pixelfields_sms'
    }

2. Add necessary settings to your app settings:

    SMS_BACKEND = 'django_pixelfields_sms.backends.gts.SmsBackend'

    SMS_HOST = 'https://sms.gateway.com/'

    SMS_HOST_USER = 'user'

    SMS_HOST_PASSWORD = 'password'

    SMS_PORT = ''

    SMS_TESTING = False

3. Run `python manage.py migrate` to create models.

4. Simple send message

    from django_pixelfields_sms import send_sms

    send_sms('Test sms message', '732178389')
