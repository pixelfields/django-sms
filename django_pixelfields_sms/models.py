from django.db import models
from django.utils.translation import ugettext_lazy as _


class SmsLog(models.Model):

    STATUS = (
        (0, _('Error')),
        (1, _('Success')),
    )

    action_time = models.DateTimeField(auto_now=True)
    provider = models.CharField(max_length=200, null=True, blank=True)
    status = models.IntegerField(choices=STATUS, default=STATUS[0][0])
    reference = models.CharField(max_length=200, null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    text_message = models.CharField(max_length=800, null=True, blank=True)
    credit = models.FloatField(null=True, blank=True)
    error_code = models.IntegerField(null=True, blank=True)
    phone = models.CharField(max_length=60, null=True, blank=True, default=None)

    class Meta:
        db_table = 'sms_log'

