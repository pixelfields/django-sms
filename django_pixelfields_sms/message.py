from unidecode import unidecode


class SmsMessage(object):

    def __init__(self, sms_message='', recipient=None, connection=None):

        self.connection = connection
        self.sms_message = sms_message
        self.recipient = recipient

    def sanitize_message(self, sms_message):

        ret = sms_message
        if isinstance(sms_message, unicode):
            try:
                ret = unidecode(sms_message)
            except Exception as e:
                pass

        return ret

    def get_connection(self, testing=True):
        from django_pixelfields_sms import get_connection
        if not self.connection:
            self.connection = get_connection(testing=True)
        return self.connection

    def send(self, testing=True):
        """Sends sms message."""

        if not self.recipient:
            return 0

        self.sms_message = self.sanitize_message(self.sms_message)

        return self.get_connection(testing).send_message(self)