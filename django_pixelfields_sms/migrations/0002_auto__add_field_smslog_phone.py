# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SmsLog.phone'
        db.add_column('sms_log', 'phone',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=60, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SmsLog.phone'
        db.delete_column('sms_log', 'phone')


    models = {
        u'django_pixelfields_sms.smslog': {
            'Meta': {'object_name': 'SmsLog', 'db_table': "'sms_log'"},
            'action_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'credit': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'error_code': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text_message': ('django.db.models.fields.CharField', [], {'max_length': '800', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['django_pixelfields_sms']