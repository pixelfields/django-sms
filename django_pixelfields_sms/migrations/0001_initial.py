# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SmsLog'
        db.create_table('sms_log', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('action_time', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('provider', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('reference', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('text_message', self.gf('django.db.models.fields.CharField')(max_length=800, null=True, blank=True)),
            ('credit', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('error_code', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'django_pixelfields_sms', ['SmsLog'])


    def backwards(self, orm):
        # Deleting model 'SmsLog'
        db.delete_table('sms_log')


    models = {
        u'django_pixelfields_sms.smslog': {
            'Meta': {'object_name': 'SmsLog', 'db_table': "'sms_log'"},
            'action_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'credit': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'error_code': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'reference': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text_message': ('django.db.models.fields.CharField', [], {'max_length': '800', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['django_pixelfields_sms']