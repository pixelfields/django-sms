"""
Tools for sending sms.
"""
from django.conf import settings
from django.utils.module_loading import import_by_path

from django_pixelfields_sms.message import SmsMessage


def get_connection(backend=None, testing=True, **kwds):

    klass = import_by_path(backend or getattr(settings, 'SMS_BACKEND'))
    return klass(testing=True, **kwds)


def send_sms(sms_message, recipient, testing=True, auth_user=None, auth_password=None, connection=None):

    connection = connection or get_connection(username=auth_user,
                                              password=auth_password,
                                              testing=testing)
    return SmsMessage(sms_message, recipient, connection=connection).send()
