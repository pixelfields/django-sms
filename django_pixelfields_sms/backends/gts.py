"""GTS sms backend class."""
import threading
import requests

from django.conf import settings

from django_pixelfields_sms.backends.base import BaseSmsBackend
from django_pixelfields_sms.models import SmsLog


class SmsBackend(BaseSmsBackend):

    RETURN_CODES_OK = ('200', '201', '202',)

    RETURN_CODES_FAIL = ('209', '301', '302', '303', '304', '305', '307', '308', '309', '310', '311', '312', '313',)

    RETURN_CODES = RETURN_CODES_OK + RETURN_CODES_FAIL

    PROVIDER = 'gts'

    def __init__(self, host=None, port=None, username=None, password=None, testing=True, **kwargs):

        super(SmsBackend, self).__init__(**kwargs)
        self.host = host or getattr(settings, 'SMS_HOST')
        self.port = port or getattr(settings, 'SMS_PORT')
        self.username = getattr(settings, 'SMS_HOST_USER') if username is None else username
        self.password = getattr(settings, 'SMS_HOST_PASSWORD') if password is None else password
        self.testing = getattr(settings, 'SMS_TESTING') if hasattr(settings, 'SMS_TESTING') is True else testing

        self.testing_recipient = '999999999'

        self._lock = threading.RLock()

    def open(self):
        if self.port is not None and self.port != '':
            port = ':%s' % self.port
        else:
            port = ''
        url = '%s%s' % (self.host, port)

        return url

    def _set_auth_string(self):
        if self.username is not None:
            username = self.username
        else:
            username = ''
        if self.password is not None:
            password = self.password
        else:
            password = ''

        auth_string = '%s:%s' % (username, password)

        return auth_string

    def send_message(self, sms_message):

        if not sms_message:
            return

        with self._lock:
            self._send(sms_message.sms_message, sms_message.recipient)

    def _send(self, sms_message, recipient):
        log_recipient = recipient
        if self.testing is True:
            recipient = self.testing_recipient

        payload = {
            'auth': self._set_auth_string(),
            'receiver': recipient,
            'smstext': sms_message,
        }

        response = requests.get(self.open(), params=payload, verify=False)
        if response.status_code == requests.codes.ok:
            return self._parse_response(response.text, sms_message, log_recipient)
        else:
            return 0

    def _parse_response(self, response, sms_message, recipient):

        p = response.splitlines()

        if len(p) > 0:
            msg_code = p[0].strip()[:3]
            if msg_code in self.RETURN_CODES_OK:
                error_code = None
                ret = 1
            else:
                error_code = msg_code
                ret = 0

            self._log(ret, sms_message, recipient, error_code=error_code)
        else:
            return 0

    def _log(self, status, sms_message, recipient, reference=None, price=None, credit=None, error_code=None):

        try:
            SmsLog.objects.create(
                provider=self.PROVIDER,
                status=status,
                reference=reference,
                price=price,
                text_message=sms_message,
                credit=credit,
                error_code=error_code,
                phone=recipient
            )
        except Exception as e:
            print e