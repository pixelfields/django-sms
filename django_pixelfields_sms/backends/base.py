"""Base sms backend class."""


class BaseSmsBackend(object):

    def __init__(self, **kwargs):

        pass

    def open(self):

        pass

    def close(self):

        pass

    def send_message(self, sms_message):

        raise NotImplementedError
